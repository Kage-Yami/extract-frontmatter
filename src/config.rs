//! Configuration options that can be applied to [`Extractor`].
//!
//! [`Extractor`]: crate::Extractor

/// Ways of distinguishing front-matter from the rest of the data in some text.
#[derive(Debug)]
#[non_exhaustive]
pub enum Splitter<'opt> {
    /// All lines before the first line matching this text (ignoring leading and trailing whitespace differences) will
    /// be treated as front-matter; everything else (excluding this line, as it is discarded) will be returned as data.
    /// If no line is found matching this text, then everything will be treated as front-matter.
    DelimiterLine(&'opt str),

    /// All lines before the second line matching this text (ignoring leading and trailing whitespace differences) will
    /// be treated as front-matter (except for the first line if it also matches this text); everything else (excluding
    /// the second matched line, as it is discarded) will be returned as data. If there is no second line matching this
    /// text, then everything will be treated as front-matter (with the first line possibly removed).
    EnclosingLines(&'opt str),

    /// All lines before this line will be treated as front-matter; everything else (including this line) will be
    /// returned as data. If there are less lines than specified here, then everything will be treated as front-matter.
    LineIndex(usize),

    /// All lines prefixed with this text, up to (and not including) the first line that is not prefixed, will be
    /// treated as front-matter; everything else will be returned as data. If no lines are found with this prefix, then
    /// everything will be treated as data. Note that the prefix will _not_ be stripped automatically; use
    /// [`Modifier::StripPrefix`] to explicitly strip the prefix.
    LinePrefix(&'opt str),
}

/// Modifiers that can be applied to extracted front-matter. Note that these will implicitly normalise newlines to just
/// `\n` (LF).
#[derive(Debug)]
#[non_exhaustive]
pub enum Modifier<'opt> {
    /// Strip the first line from the front-matter returned.
    StripFirstLine,

    /// Strip the last line from the front-matter returned.
    StripLastLine,

    /// Strips the given prefix from the start of all front-matter lines.
    StripPrefix(&'opt str),

    /// Strip leading and trailing whitespace from all front-matter lines.
    TrimWhitespace,
}
