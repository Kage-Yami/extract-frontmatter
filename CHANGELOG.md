# Changelog <!-- omit in toc -->

- [4.x.x](#4xx)
    - [4.1.1 - 22-Apr-2022](#411---22-apr-2022)
    - [4.1.0 - 19-Apr-2022](#410---19-apr-2022)
    - [4.0.0 - 19-Apr-2022](#400---19-apr-2022)
- [3.x.x](#3xx)
    - [3.0.2 - 19-Apr-2022](#302---19-apr-2022)
    - [3.0.1 - 19-Apr-2022](#301---19-apr-2022)
    - [3.0.0 - 19-Apr-2022](#300---19-apr-2022)
- [2.x.x](#2xx)
    - [2.1.0 - 21-Jan-2021](#210---21-jan-2021)
    - [2.0.3 - 02-Jan-2020](#203---02-jan-2020)
    - [2.0.2 - 02-Jan-2021](#202---02-jan-2021)
    - [2.0.1 - 02-Jan-2021](#201---02-jan-2021)
    - [2.0.0 - 15-Aug-2020](#200---15-aug-2020)
- [1.x.x](#1xx)
    - [1.0.3 - 14-Aug-2020](#103---14-aug-2020)
    - [1.0.2 - 24-May-2020](#102---24-may-2020)
    - [1.0.1 - 03-May-2020](#101---03-may-2020)
    - [1.0.0 - 02-Mar-2020](#100---02-mar-2020)
- [Pre-release versions](#pre-release-versions)
    - [0.6.2 - 02-May-2020](#062---02-may-2020)

## 4.x.x

### 4.1.1 - 22-Apr-2022

- ➕ Add MSRV to `Cargo.toml` and verify as part of CI.

### 4.1.0 - 19-Apr-2022

- ➕ Add `Splitter:EnclosingLines(&str)`
    - Replaces old arguably-buggy behaviour from `v2.1.0` where `.select_by_terminator(&str)` would ignore the first
      terminator if it was on the very first line of input
    - 🪲 Fixes [#4]

[#4]: https://gitlab.com/isekai/libraries/rust/extract-frontmatter/-/issues/4

### 4.0.0 - 19-Apr-2022

- 📝 Update `Splitter` and `Modifier` enum variants that used `String`s to now use `&str`
    - Should make creating literals of them a little bit easier
    - (Meant to give this a go for `v3.x.x`, but forgot...)

## 3.x.x

### 3.0.2 - 19-Apr-2022

- 🪲 Fix "dependents" badge in `README.md`

### 3.0.1 - 19-Apr-2022

- 🪲 `README.md` fixes

### 3.0.0 - 19-Apr-2022

- 🌟 Complete rewrite!
    - `Extractor::new()` now takes a `Splitter`, rather than the input string to extract front-matter from
        - Replaces all `.select_by_*()` methods
    - Modifier methods are replaced by a `Modifier` enum and the `.add_modifier()` method
    - `.extract()` now takes the input string to split, and returns a tuple of the front-matter and data as strings
        - Replaces `.collect()`, `.remove()`, and `.split()`

## 2.x.x

### 2.1.0 - 21-Jan-2021

- ➕ Added `Extractor::remove` to return everything _except_ the matched frontmatter
- ➕ Added `Extractor::split` to return a tuple of the front-matter and document content

### 2.0.3 - 02-Jan-2020

_Note: no changes have been made to the library itself in this version._

- 📝 Helps to also bump the crate version

### 2.0.2 - 02-Jan-2021

_Notes:_

- _No changes have been made to the library itself in this version._
- _Not published to https://crates.io_

Changes:

- 📝 Fix up CI to correctly compare version with tag

### 2.0.1 - 02-Jan-2021

_Notes:_

- _No changes have been made to the library itself in this version._
- _Not published to https://crates.io_

Changes:

- ➕ Added links in documentation
- 📝 Minor internal project refactor

### 2.0.0 - 15-Aug-2020

- 🌟 Complete refactor:
    - Configuration now set via a "fluent" interface (chained method calls)
    - Future configuration options won't result in breaking changes
    - No auto-detection of front-matter (explicit configuration now required)
- ➕ New modifiers:
    - Strip leading and trailing whitespace
    - Discard last line
- ➕ Can now collect front-matter into a `Vec<&str>`

## 1.x.x

### 1.0.3 - 14-Aug-2020

- 📝 (Internal) Return `&str` - not `String` - in `clean` function
- ➖ Removed `UnicodeSegmentation` dependency

### 1.0.2 - 24-May-2020

_Note: no changes have been made to the library itself in this version._

- 🪲 Fixed typo in readme
- 📝 Tweaked CI/CD
    - Don't create local tags on non-`master` branch runs
    - Rebase `rc` and `dev` onto `master` on `master` branch runs

### 1.0.1 - 03-May-2020

_Note: no changes have been made to the library itself in this version._

- 📝 Updated maintenance status

### 1.0.0 - 02-Mar-2020

- 🌟 Initial release and publication to [crates.io]

[crates.io]: https://crates.io/extract-frontmatter

## Pre-release versions

### 0.6.2 - 02-May-2020

- 🌟 Initial pre-release version
