#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]

use extract_frontmatter::config::{Modifier, Splitter};
use extract_frontmatter::Extractor;

#[test]
fn markdown_with_toml() {
    let (actual_meta, actual_data) =
        Extractor::new(Splitter::DelimiterLine("+++")).extract(include_str!("../resources/tests/example1/raw.md"));

    assert_eq!(
        (actual_meta.trim(), actual_data.trim()),
        (
            include_str!("../resources/tests/example1/meta.toml").trim(),
            include_str!("../resources/tests/example1/data.md").trim()
        )
    );
}

#[test]
fn sql_with_yaml() {
    let (actual_meta, actual_data) = Extractor::new(Splitter::LinePrefix("-- "))
        .with_modifier(Modifier::StripPrefix("-- "))
        .extract(include_str!("../resources/tests/example2/raw.sql"));

    assert_eq!(
        (actual_meta.trim(), actual_data.trim()),
        (
            include_str!("../resources/tests/example2/meta.yml").trim(),
            include_str!("../resources/tests/example2/data.sql").trim()
        )
    );
}
