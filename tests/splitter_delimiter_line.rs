#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]

use extract_frontmatter::config::Splitter;
use extract_frontmatter::Extractor;
use std::borrow::Cow;

#[test]
fn zero_lines_empty_delim() {
    assert_eq!(Extractor::new(Splitter::DelimiterLine("")).extract(""), (Cow::Borrowed(""), ""));
}

#[test]
fn zero_lines_missing_delim() {
    assert_eq!(Extractor::new(Splitter::DelimiterLine("---")).extract(""), (Cow::Borrowed(""), ""));
}

#[test]
fn one_line_empty_delim() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("")).extract("Front-matter line"),
        (Cow::Borrowed("Front-matter line"), "")
    );
}

#[test]
fn one_line_missing_delim() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("---")).extract("Front-matter line"),
        (Cow::Borrowed("Front-matter line"), "")
    );
}

#[test]
fn one_line_delim_first() {
    assert_eq!(Extractor::new(Splitter::DelimiterLine("---")).extract("---"), (Cow::Borrowed(""), ""));
}

#[test]
fn two_lines_empty_delim() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("")).extract(concat!("Front-matter line 1\n", "Front-matter line 2")),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2")), "")
    );
}

#[test]
fn two_lines_empty_delim_first_line_empty() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("")).extract(concat!("\n", "Data line")),
        (Cow::Borrowed(""), "Data line")
    );
}

#[test]
fn two_lines_empty_delim_second_line_empty() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("")).extract(concat!("Front-matter line\n", "")),
        (Cow::Borrowed("Front-matter line\n"), "")
    );
}

#[test]
fn two_lines_missing_delim() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("---")).extract(concat!("Front-matter line 1\n", "Front-matter line 2")),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2")), "")
    );
}

#[test]
fn two_lines_delim_first() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("---")).extract(concat!("---\n", "Data line")),
        (Cow::Borrowed(""), "Data line")
    );
}

#[test]
fn two_lines_delim_second() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("---")).extract(concat!("Front-matter line\n", "---")),
        (Cow::Borrowed("Front-matter line\n"), "")
    );
}

#[test]
fn three_lines_empty_delim() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("")).extract(concat!(
            "Front-matter line 1\n",
            "Front-matter line 2\n",
            "Front-matter line 3"
        )),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")), "")
    );
}

#[test]
fn three_lines_empty_delim_first_line_empty() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("")).extract(concat!("\n", "Data line 1\n", "Data line 2")),
        (Cow::Borrowed(""), concat!("Data line 1\n", "Data line 2"))
    );
}

#[test]
fn three_lines_empty_delim_second_line_empty() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("")).extract(concat!("Front-matter line\n", "\n", "Data line")),
        (Cow::Borrowed("Front-matter line\n"), "Data line")
    );
}

#[test]
fn three_lines_empty_delim_third_line_empty() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("")).extract(concat!(
            "Front-matter line 1\n",
            "Front-matter line 2\n",
            ""
        )),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2\n")), "")
    );
}

#[test]
fn three_lines_missing_delim() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("---")).extract(concat!(
            "Front-matter line 1\n",
            "Front-matter line 2\n",
            "Front-matter line 3"
        )),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")), "")
    );
}

#[test]
fn three_lines_delim_first() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("---")).extract(concat!("---\n", "Data line 1\n", "Data line 2")),
        (Cow::Borrowed(""), concat!("Data line 1\n", "Data line 2"))
    );
}

#[test]
fn three_lines_delim_second() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("---")).extract(concat!("Front-matter line\n", "---\n", "Data line")),
        (Cow::Borrowed("Front-matter line\n"), "Data line")
    );
}

#[test]
fn three_lines_delim_third() {
    assert_eq!(
        Extractor::new(Splitter::DelimiterLine("---")).extract(concat!(
            "Front-matter line 1\n",
            "Front-matter line 2\n",
            "---"
        )),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2\n")), "")
    );
}
