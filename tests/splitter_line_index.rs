#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]

use extract_frontmatter::config::Splitter;
use extract_frontmatter::Extractor;
use std::borrow::Cow;

#[test]
fn zero_lines_index_zero() {
    assert_eq!(Extractor::new(Splitter::LineIndex(0)).extract(""), (Cow::Borrowed(""), ""));
}

#[test]
fn zero_lines_index_one() {
    assert_eq!(Extractor::new(Splitter::LineIndex(1)).extract(""), (Cow::Borrowed(""), ""));
}

#[test]
fn one_line_index_zero() {
    assert_eq!(Extractor::new(Splitter::LineIndex(0)).extract("Example data"), (Cow::Borrowed(""), "Example data"));
}

#[test]
fn one_line_index_one() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(1)).extract("Example front-matter"),
        (Cow::Borrowed("Example front-matter"), "")
    );
}

#[test]
fn one_line_index_two() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(2)).extract("Example front-matter"),
        (Cow::Borrowed("Example front-matter"), "")
    );
}

#[test]
fn two_lines_index_zero() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(0)).extract(concat!("Data line 1\n", "Data line 2")),
        (Cow::Borrowed(""), concat!("Data line 1\n", "Data line 2"))
    );
}

#[test]
fn two_lines_index_one() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(1)).extract(concat!("Front-matter line\n", "Data line")),
        (Cow::Borrowed("Front-matter line\n"), "Data line")
    );
}

#[test]
fn two_lines_index_two() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(2)).extract(concat!("Front-matter line 1\n", "Front-matter line 2")),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2")), "")
    );
}

#[test]
fn two_lines_index_three() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(3)).extract(concat!("Front-matter line 1\n", "Front-matter line 2")),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2")), "")
    );
}

#[test]
fn three_lines_index_zero() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(0)).extract(concat!("Data line 1\n", "Data line 2\n", "Data line 3")),
        (Cow::Borrowed(""), concat!("Data line 1\n", "Data line 2\n", "Data line 3"))
    );
}

#[test]
fn three_lines_index_one() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(1)).extract(concat!("Front-matter line\n", "Data line 1\n", "Data line 2")),
        (Cow::Borrowed("Front-matter line\n"), concat!("Data line 1\n", "Data line 2"))
    );
}

#[test]
fn three_lines_index_two() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(2)).extract(concat!(
            "Front-matter line 1\n",
            "Front-matter line 2\n",
            "Data line"
        )),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2\n")), "Data line")
    );
}

#[test]
fn three_lines_index_three() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(3)).extract(concat!(
            "Front-matter line 1\n",
            "Front-matter line 2\n",
            "Front-matter line 3"
        )),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")), "")
    );
}

#[test]
fn three_lines_index_four() {
    assert_eq!(
        Extractor::new(Splitter::LineIndex(4)).extract(concat!(
            "Front-matter line 1\n",
            "Front-matter line 2\n",
            "Front-matter line 3"
        )),
        (Cow::Borrowed(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")), "")
    );
}
