#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]

use extract_frontmatter::config::{Modifier, Splitter};
use extract_frontmatter::Extractor;
use std::borrow::Cow;

#[test]
fn no_whitespace() {
    let (actual, _) = Extractor::new(Splitter::LineIndex(3)).with_modifier(Modifier::TrimWhitespace).extract(concat!(
        "Front-matter line 1\n",
        "Front-matter line 2\n",
        "Front-matter line 3"
    ));

    let expected: Cow<str> =
        Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")));

    assert_eq!(actual, expected);
}

#[test]
fn leading_whitespace() {
    let (actual, _) = Extractor::new(Splitter::LinePrefix("--"))
        .with_modifier(Modifier::StripPrefix("--"))
        .with_modifier(Modifier::TrimWhitespace)
        .extract(concat!("-- Front-matter line 1\n", "-- Front-matter line 2\n", "-- Front-matter line 3"));

    let expected: Cow<str> =
        Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")));

    assert_eq!(actual, expected);
}

#[test]
fn trailing_whitespace() {
    let (actual, _) = Extractor::new(Splitter::LineIndex(3)).with_modifier(Modifier::TrimWhitespace).extract(concat!(
        "Front-matter line 1\t\n",
        "Front-matter line 2\t\n",
        "Front-matter line 3\t"
    ));

    let expected: Cow<str> =
        Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")));

    assert_eq!(actual, expected);
}

#[test]
fn leading_and_trailing_whitespace() {
    let (actual, _) = Extractor::new(Splitter::LinePrefix("--"))
        .with_modifier(Modifier::StripPrefix("--"))
        .with_modifier(Modifier::TrimWhitespace)
        .extract(concat!("-- Front-matter line 1\t\n", "-- Front-matter line 2\t\n", "-- Front-matter line 3\t"));

    let expected: Cow<str> =
        Cow::Owned(String::from(concat!("Front-matter line 1\n", "Front-matter line 2\n", "Front-matter line 3")));

    assert_eq!(actual, expected);
}
